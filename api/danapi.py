#!/usr/bin/env python2

# Danbooru / Moebooru API client for use with Kodi
# ------------------------------------------------
#  Author : Markus Koch <markus@notsyncing.net>
# Created : 2014/08/15
# ------------------------------------------------
# Released under the GNU GPL V.3 License as published
# by the Free Software Foundation.

import sys, urllib
import xml.etree.ElementTree as ET
import simplejson

class danapi_mod_moebooru(object):
	"""Konachan Module for danapi"""
	def __init__(self, server, basepath, default_order = 0, default_rating = 0, default_tagOrder = 0, pagesize = 5, tagpagesize = 0):
		self.server = server
		self.basepath = basepath # has to end in a /

		self.file_posts = "post.xml" # XML should *always* be preferred
		self.file_tagsearch = "tag.xml"
		self.file_relatedTags = "tag/related.xml"
		self.file_listPools = "pool.xml"
		self.file_getPool = "pool/show.xml"
		self.path_prefix = "http:"

		# Image urls
		self.tag_preview = "preview_url"
		self.tag_jpeg = "jpeg_url"
		self.tag_image = "file_url"
		self.tag_tags = "tags"
		self.tag_author = "author"
		self.tag_imageCount = "count" # (global) How many pictures exist for a search tag
		self.tag_id = "id"

		# Tags
		self.tag_tagname = "name"

		# Pools
		self.tag_poolName = "name"
		self.tag_poolDescription = "description"
		self.tag_poolCount = "post_count" # Note this is different from normal searches
		self.tag_poolId = "id"

		self.default_order = default_order
		self.default_rating = default_rating
		self.default_tagOrder = default_tagOrder

		self.pRating = ["", "safe", "questionable", "explicit", "questionableplus", "questionableless"]
		self.pOrder = ["", "score", "fav", "wide", "nonwide"]
		self.pTagOrder = ["", "date", "count", "name"]		

		self.pagesize = pagesize
		self.tagpagesize = tagpagesize # 0 means unlimited

	def paramsToUrl(self, query, page):
		# First, specify rating and order
		if (query.find(" order:") == -1): # If the user has not requested a specific order
			if (self.default_order > 0): # anything != "server default" (0)
				query += " order:" + self.pOrder[self.default_order]
		if (query.find(" rating:") == -1):
			if (self.default_rating > 0):
				query += " rating:" + self.pRating[self.default_rating]
		# Now assemble the GET-Request
		query = "tags=" + urllib.quote_plus(query) + "&page=" + str(page) + "&limit=" + str(self.pagesize)
		return query

	def tagSearchToUrl(self, query, page):
		query = "name=" + urllib.quote_plus(query) + "&page=" + str(page)
		if (self.default_tagOrder > 0):
			query += "&order=" + self.pTagOrder[self.default_tagOrder]
		query += "&limit=" + str(self.tagpagesize)
		return query

	def relatedToUrl(self, query):
		query = "tags=" + urllib.quote_plus(query)
		return query

	def listPoolsToUrl(self, query, page):
		query = "query=" + urllib.quote_plus(query) + "&page=" + str(page)
		return query

	def poolToUrl(self, id, page):
		query = "id=" + str(id) + "&page=" + str(page)
		return query

class danapi_result(object):
	"""Returned by every command"""
	def __init__(self):
		self.success = 0
		self.error = "Function returned an empty result."
		self.result = []
	def ok(self, data):
		self.success = 1
		self.error = "No error."
		self.result = data
	def fail(self, error="Unknown error. See log for details."):
		self.success = 0
		self.error = error
		self.result = error # TODO: Replace with an empty value after debugging

class danapi(object):
	"""Danbooru / Moebooru API"""

	# JSON Compatibility
	def isJsonUrl(self, url):
		if ".json" in url:
			return 1
		else:
			return 0

	def jsonToXML(self, json, tagNames=["root", "tag"]):
		result = ET.Element(tagNames[0], {"count" : str(99999999)}) # Bodge fix to get it somewhat running

		if tagNames[1] == "related": # yea this function becomes bodgier with every revision
			for el in json:
				attribs = {}
				attribs.update({"name" : str(el[0])})
				attribs.update({"count" : str(el[1])})
				ET.SubElement(result, tagNames[0], attribs)
		else:
			for el in json:
				attribs = {}
				for sl in el:
					#print sl + "=" + str(el.get(sl))
					attribs.update({str(sl) : unicode(el.get(sl))});
				ET.SubElement(result, tagNames[1], attribs)

		return result

	# Generic functions
	def __danapi_debug(self, errMessage, severity=0):
		if (severity >= self.debugLevel):
			print "\033[95m<DANAPI>" + self.debugLevelColorCodes[severity]  + " [" + self.debugLevelNames[severity] + "] " + errMessage + "\033[0m"

	def __init__(self, danapi_module, debugLevel=0):
		#super(danapi, self).__init__()
		self.debugLevel = debugLevel
		self.debugLevelNames = ["*DEBUG", " NOTE ", " WARN ", "!ERROR"]
		self.debugLevelColorCodes = ["\033[94m", "\033[92m", "\033[93m\033[1m", "\033[91m\033[1m"]
		self.danapi_module = danapi_module

	# General info
	def __getPostXML(self, query, page):
		url = self.danapi_module.path_prefix + "//" + self.danapi_module.server + "/" + self.danapi_module.basepath + self.danapi_module.file_posts + "?" + self.danapi_module.paramsToUrl(query, page)
		self.__danapi_debug("HTTP GET: " + url)
		raw = urllib.urlopen(url)
		if self.isJsonUrl(url):
			if page == 0: # Page = 0 when called by getPostCount
				self.__danapi_debug("Post count not supported by JSON! Returning arbitrary value.", 2)
			xml = self.jsonToXML(simplejson.loads(raw.read()), ["posts", "post"])
		else:
			xml = ET.ElementTree(ET.fromstring(raw.read())).getroot()

		if xml.tag != "posts":
			self.__danapi_debug("getPost: Server returned invalid XML", 3)

		return xml

	# Get the total amount of posts availabe for a certain search
	def getPostCount(self, query):
		result = danapi_result()
		xml = self.__getPostXML(query, 0) # TODO: We could limit this to one result to save bandwidth
		if xml.tag == "posts" and self.danapi_module.tag_imageCount in xml.attrib:
			result.ok(xml.attrib[self.danapi_module.tag_imageCount])
		else:
			result.fail("XML result does not contain any image count information.")
		return result

	# Image search
	def getPosts(self, query, page=1): # Returns list of posts
		result = danapi_result()
		xml= self.__getPostXML(query, page)

		if len(xml) <= 0 or int(xml.attrib["count"]) <= 0:
			self.__danapi_debug("getPosts: Did not receive any posts.", 2)
			return result

		result.ok(xml.getchildren())
		return result

	# Image access (post)
	def getId(self, post):
		result = danapi_result()
		if self.danapi_module.tag_id in post.attrib:
			result.ok(post.attrib[self.danapi_module.tag_id])
		else:
			result.fail("Image id tag does not exist in XML.")
		return result
	def getPreview(self, post):
		result = danapi_result()
		if self.danapi_module.tag_preview in post.attrib:
			result.ok(self.danapi_module.path_prefix + post.attrib[self.danapi_module.tag_preview])
		else:
			result.fail("Image preview tag does not exist in XML.")
		return result
	def getJpeg(self, post):
		result = danapi_result()
		if self.danapi_module.tag_jpeg in post.attrib:
			result.ok(self.danapi_module.path_prefix + post.attrib[self.danapi_module.tag_jpeg])
		else:
			result.fail("Image jpeg tag does not exist in XML.")
		return result
	def getImage(self, post):
		result = danapi_result()
		if self.danapi_module.tag_image in post.attrib:
			result.ok(self.danapi_module.path_prefix + post.attrib[self.danapi_module.tag_image])
		else:
			result.fail("Image url tag does not exist in XML.")
		return result
	def getTags(self, post):
		result = danapi_result()
		if self.danapi_module.tag_tags in post.attrib:
			result.ok(post.attrib[self.danapi_module.tag_tags].split(" "))
		else:
			result.fail("Tag list not found in XML.")
		return result
	def getAuthor(self, post):
		result = danapi_result()
		if self.danapi_module.tag_author in post.attrib:
			result.ok(post.attrib[self.danapi_module.tag_author])
		else:
			result.fail("Author not found in XML.")
		return result

	# Tags
	def __getTagXML(self, query, page):
		url = self.danapi_module.path_prefix + "//" + self.danapi_module.server + "/" + self.danapi_module.basepath + self.danapi_module.file_tagsearch + "?" + self.danapi_module.tagSearchToUrl(query, page)
		self.__danapi_debug("HTTP GET: " + url)
		raw = urllib.urlopen(url)
		if self.isJsonUrl(url):
			xml = self.jsonToXML(simplejson.loads(raw.read()), ["tags", "tag"])
		else:
			xml = ET.ElementTree(ET.fromstring(raw.read())).getroot()
		
		if xml.tag != "tags":
			self.__danapi_debug("getTags: Server returned invalid XML", 3)

		return xml

	def searchTag(self, query):
		result = danapi_result()
		xml = self.__getTagXML(query, 1)

		if len(xml) <= 0:
			self.__danapi_debug("getPosts: Did not receive any tags.", 2)
			return result

		result.ok(xml.getchildren())
		return result

	def getTagName(self, tag):
		result = danapi_result()
		if self.danapi_module.tag_tagname in tag.attrib:
			result.ok(tag.attrib[self.danapi_module.tag_tagname])
		else:
			result.fail("Could not find tag name in tag XML")
		return result

	def getPostCountForTag(self, tag):
		result = danapi_result()
		if self.danapi_module.tag_imageCount in tag.attrib:
			result.ok(tag.attrib[self.danapi_module.tag_imageCount])
		else:
			result.fail("Could not find image count in tag XML")
		return result

	def __getRelatedXML(self, query):
		url = self.danapi_module.path_prefix + "//" + self.danapi_module.server + "/" + self.danapi_module.basepath + self.danapi_module.file_relatedTags + "?" + self.danapi_module.relatedToUrl(query)
		self.__danapi_debug("HTTP GET: " + url)
		raw = urllib.urlopen(url)
		if self.isJsonUrl(url):
			xml = self.jsonToXML(simplejson.loads(raw.read()).get(query), ["tag", "related"])
		else:
			xml = ET.ElementTree(ET.fromstring(raw.read())).getroot()[0]
		
		if xml.tag != "tag":
			self.__danapi_debug("getRelated: Server returned invalid XML", 3)

		return xml

	def getRelatedTags(self, query): # Supply only ONE tag (even though some pages allow multiple)
		result = danapi_result()
		xml = self.__getRelatedXML(query)
		if len(xml) <= 0:
			result.fail("TODO: No related tags retrieved.")
		else:
			result.ok(xml)
		return result

	# Pools
	def __getListPoolsXML(self, query, page):
		url = self.danapi_module.path_prefix + "//" + self.danapi_module.server + "/" + self.danapi_module.basepath + self.danapi_module.file_listPools + "?" + self.danapi_module.listPoolsToUrl(query, page)
		self.__danapi_debug("HTTP GET: " + url)
		raw = urllib.urlopen(url)
		if self.isJsonUrl(url):
			xml = self.jsonToXML(simplejson.loads(raw.read()), ["pools", "pool"])
		else:
			xml = ET.ElementTree(ET.fromstring(raw.read())).getroot()
		return xml

	def listPools(self, query, page = 1):
		result = danapi_result()
		xml = self.__getListPoolsXML(query, page)

		if xml.tag != "pools":
			self.__danapi_debug("listPools: Server returned invalid XML", 3)
			result.fail("XML result did not return any pools.")
		else:
			result.ok(xml.getchildren())
		return result

	def getPoolName(self, pool): # Note: Replaces underscores with spaces
		result = danapi_result()
		if self.danapi_module.tag_poolName in pool.attrib:
			result.ok(pool.attrib[self.danapi_module.tag_poolName].replace("_", " "))
		else:
			result.fail("Pool name not available")
		return result

	def getPoolId(self, pool):
		result = danapi_result()
		if self.danapi_module.tag_poolId in pool.attrib:
			result.ok(pool.attrib[self.danapi_module.tag_poolId])
		else:
			result.fail("Could not get pool ID")
			self.__danapi_debug("Could not retrieve pool ID!", 2)
		return result

	def getPoolCount(self, pool):
		result = danapi_result()
		if self.danapi_module.tag_poolCount in pool.attrib:
			result.ok(pool.attrib[self.danapi_module.tag_poolCount])
		else:
			result.fail("Pool image count not available")
		return result

	def getPoolDescription(self, pool):
		result = danapi_result()
		if self.danapi_module.tag_poolDescription in pool.attrib:
			result.ok(pool.attrib[self.danapi_module.tag_poolDescription])
		else:
			result.fail("Pool description not available")
		return result

	# Access Pools
	def __getPoolsXML(self, id, page):
		url = self.danapi_module.path_prefix + "//" + self.danapi_module.server + "/" + self.danapi_module.basepath + self.danapi_module.file_getPool + "?" + self.danapi_module.poolToUrl(id, page)
		self.__danapi_debug("HTTP GET: " + url)
		raw = urllib.urlopen(url)
		if self.isJsonUrl(url):
			xml = self.jsonToXML(simplejson.loads(raw.read()), ["posts", "post"])
		else:
			xml = ET.ElementTree(ET.fromstring(raw.read())).getroot().getchildren()[1] # traverse one level deeper
		return xml

	def getPool(self, pool, page = 1): # pool can either be an id (int) or a pool object as returned by listPools
		result = danapi_result()

		if not isinstance(pool, int):
			pool = self.getPoolId(pool).result

		xml = self.__getPoolsXML(pool, page)

		if len(xml) <= 0 or list(xml)[0].tag != "post":
			result.fail("No posts returned.")
		else:
			result.ok(xml)

		return result # Pools use the same data access functions as search results

'''
# ---------- DEBUGGING / TESTING ---------- #

mod = danapi_mod_moebooru("konachan.com", "") # Set konachan.com as client page (using moebooru api)
moe = danapi(mod) # Load Moebooru client

#quit(1)

print ""
print "[TEST] POOL ACCESS"
pool = moe.getPool(9)
if pool.success:
	for post in pool.result:
		print "    Preview = " + moe.getPreview(post).result
		print "    Jpeg    = " + moe.getJpeg(post).result
		print "    URL     = " + moe.getImage(post).result
		print "    Author  = " + moe.getAuthor(post).result
		print "---"

print ""
print "[TEST] POOL SEARCH"
pools = moe.listPools("clannad")
if pools.success:
	for pool in pools.result:
		print "[" + moe.getPoolId(pool).result + "] " + moe.getPoolCount(pool).result + " @ " + moe.getPoolName(pool).result
		print "    " + moe.getPoolDescription(pool).result

print ""
print "[TEST] TAG SEARCH"
tags = moe.getRelatedTags("yuri")
if tags.success:
	for tag in tags.result:
		print moe.getTagName(tag).result + " @ " + moe.getPostCountForTag(tag).result
else:
	print tags.error

print ""
print "[TEST] TAG SEARCH"
tags = moe.searchTag("yuri")
if tags.success:
	for tag in tags.result:
		print moe.getTagName(tag).result + " @ " + moe.getPostCountForTag(tag).result


print ""
print "[TEST] POST COUNT"
count = moe.getPostCount("yuri")
if (count.success):
	print "Found " + str(count.result) + " images."


print ""
print "[TEST] GET URL"
posts = moe.getPosts("") # Get posts for query
if posts.success:
	for post in posts.result:
		print "    Preview = " + moe.getPreview(post).result
		print "    Jpeg    = " + moe.getJpeg(post).result
		print "    URL     = " + moe.getImage(post).result
		print "    Author  = " + moe.getAuthor(post).result
		sys.stdout.write("    Tags    = ")
		taglist = moe.getTags(post).result
		for tag in taglist:
			sys.stdout.write(tag + " ")
		print ""
		print "---"
'''
