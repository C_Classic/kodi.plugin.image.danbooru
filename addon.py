#!/usr/bin/env python2

# Danbooru / Moebooru API client for use with Kodi
# ------------------------------------------------
#  Author : Markus Koch <markus@notsyncing.net>
# Created : 2016/12/28
# ------------------------------------------------
# Released under the GNU GPL V.3 License as published
# by the Free Software Foundation.

import sys
import urllib
import urlparse
import xbmcgui
import xbmcplugin
import xbmcaddon
import os
import ast
from api import danapi

""" INITIALIZATION """

# Init basic variables to access Kodi
base_url = sys.argv[0]
addon_handle = int(sys.argv[1])
args = urlparse.parse_qs(sys.argv[2][1:])

__settings__ = xbmcaddon.Addon()
IMAGE_PATH = os.path.join(xbmc.translatePath(__settings__.getAddonInfo('path')),'resources','images')
language = __settings__.getLocalizedString

xbmcplugin.setContent(addon_handle, 'image')

# Init stuff for adressing
def build_url(query):
	return base_url + '?' + urllib.urlencode(query)

mode  = args.get('mode', None)
page  = args.get('page', [1])[0]
query = args.get('query', [""])[0]

# Init danapi
mod = danapi.danapi_mod_moebooru(__settings__.getSetting('server'), "",
	  int(__settings__.getSetting('order')), int(__settings__.getSetting('rating')), int(__settings__.getSetting('tagOrder')),
	  int(__settings__.getSetting('epp')), int(__settings__.getSetting('maxTags'))
)

moe = danapi.danapi(mod) # Load Moebooru client

POOLS_PER_PAGE = 20

""" INITIALIZATION END """

def notify(title, text):
	xbmc.executebuiltin("Notification(" + title + "," + text + ")")

""" Menus """
def menu_add_directory(caption, iconImage, url, tot=0):
	url = build_url(url)
	if iconImage[:4] != "http":
		iconImage = os.path.join(IMAGE_PATH,iconImage + ".png");
	li = xbmcgui.ListItem(caption, iconImage=iconImage, thumbnailImage=iconImage)
	xbmcplugin.addDirectoryItem(handle=addon_handle, url=url, listitem=li, isFolder=True, totalItems=tot)

def menu_add_tag(tag, count, tot=0):
	count = int(count)
	previewUrl = 'tags'
	if count > 0:
		caption = tag + " (" + str(count) + ")"
	else:
		caption = tag
	if str(__settings__.getSetting('previewTags')) == "true":
		posts = moe.getPosts(tag + " order:random", 1) # TODO: We could only request one image for this to lighten server load; order:random prevents the latest image for all tags
		if posts.success and len(posts.result) > 0:
			previewUrl = moe.getPreview(posts.result[0]).result
	menu_add_directory(caption, previewUrl, {'mode': 'search', 'query': tag}, tot);

def menu_add_pool(pool, tot):
	if tot > 0:
		previewUrl = 'pools'
		if str(__settings__.getSetting('previewPools')) == "true":
			posts = moe.getPool(int(moe.getPoolId(pool).result))
			if posts.success and len(posts.result) > 0:
				previewUrl = moe.getPreview(posts.result[0]).result
		menu_add_directory(moe.getPoolName(pool).result + " (" + moe.getPoolCount(pool).result + ")", previewUrl, {'mode': 'pool', 'query': moe.getPoolId(pool).result}, tot);

def menu_add_post(post):
	li = xbmcgui.ListItem(moe.getId(post).result, iconImage=moe.getPreview(post).result)
	li.setInfo( type="image", infoLabels={ "Id": moe.getId(post).result })
	contextMenu = [(language(30601), xbmc.translatePath('Container.Update(' + build_url({'mode': 'taglist', 'query': moe.getTags(post).result}) + ')'))]
	li.addContextMenuItems(contextMenu)
	xbmcplugin.addDirectoryItem(handle=addon_handle, url=moe.getJpeg(post).result, listitem=li)

def menu_main():
	menu_add_directory(language(30500),	'latest'  ,	{'mode': 'search'    , 'query': ''});
	menu_add_directory(language(30501),	'search'  ,	{'mode': 'keyboard'  , 'caption' : language(30700), 'jumpmode': 'search'});
	menu_add_directory(language(30502),	'random'  ,	{'mode': 'search'    , 'query': 'order:random'});
	menu_add_directory(language(30503),	'pools'   ,	{'mode': 'poolsearch', 'query': ''});
	menu_add_directory(language(30504),	'tags'  ,	{'mode': 'keyboard'  , 'caption' : language(30700), 'jumpmode': 'tagsearch'});
	#menu_add_directory(language(30505),	'history' ,	{'mode': 'history'});

def menu_search():
	posts = moe.getPosts(query, page)
	if posts.success and (page != 1 or len(posts.result) > 0): # When first page has results
		if page == 1 and query != "" and query != "order:random":
			menu_add_directory(language(30521),	'tags', {'mode': 'related', 'query': query});
		for post in posts.result:
			menu_add_post(post)
		if len(posts.result) == int(__settings__.getSetting('epp')): # MoreAvailable?
			menu_add_directory(language(30520),	'more', {'mode': 'search', 'query': query, 'page': str(int(page) + 1)});
	else:
		#xbmc.log("FAILFAILFIAL", xbmc.LOGERROR)
		notify(language(30800), language(30801)) # No results, show suggested tags
		menu_tagsearch()

def menu_related():
	tags = moe.getRelatedTags(query)
	if tags.success and len(tags.result) > 0:
		for tag in tags.result:
			menu_add_tag(moe.getTagName(tag).result, moe.getPostCountForTag(tag).result, tot=len(tags.result))
	else:
		notify(language(30800), language(30802)) # No results

def menu_tagsearch():
	tags = moe.searchTag(query)
	if tags.success and len(tags.result) > 0:
		for tag in tags.result:
			menu_add_tag(moe.getTagName(tag).result, moe.getPostCountForTag(tag).result, tot=len(tags.result))
	else:
		notify(language(30800), language(30802)) # No results

def menu_taglist():
	tags = ast.literal_eval(query)
	for tag in tags:
		menu_add_tag(tag, -1, len(tags))

def menu_poolsearch():
	if page == 1 and query == '':
		menu_add_directory(language(30505),	'search'  ,	{'mode': 'keyboard'  , 'caption' : language(30701), 'jumpmode': 'poolsearch'})
	pools = moe.listPools(query, page)
	if pools.success and len(pools.result) > 0:
		for pool in pools.result:
			menu_add_pool(pool, len(pools.result))
		if len(pools.result) == POOLS_PER_PAGE: # MoreAvailable?
			menu_add_directory(language(30520),	'more', {'mode': 'poolsearch', 'query': query, 'page': str(int(page) + 1)});
	else:
		notify(language(30800), language(30802)) # No results

def menu_pool():
	posts = moe.getPool(int(query))
	if posts.success and len(posts.result) > 0: # When first page has results
		for post in posts.result:
			menu_add_post(post)
	else:
		notify(language(30800), language(30802)) # No results

""" MAIN CODE ENTRY POINT """
if mode is None:
	menu_main()
elif mode[0] == 'search':
	menu_search()
elif mode[0] == 'related':
	menu_related()
elif mode[0] == 'tagsearch':
	menu_tagsearch()
elif mode[0] == 'taglist':
	menu_taglist()
elif mode[0] == 'poolsearch':
	menu_poolsearch()
elif mode[0] == 'pool':
	menu_pool()
elif mode[0] == 'keyboard': # Keyboard Jump Handler
	kbd = xbmc.Keyboard('', args.get('caption', [""])[0])
	kbd.doModal()
	if (kbd.isConfirmed()):
		xbmc.executebuiltin("Container.Refresh(" + build_url({'mode': args['jumpmode'][0], 'query': kbd.getText()}) + ")")
	quit(0)
else:
	xbmc.executebuiltin("Notification(Error, Not yet implemented)")
xbmcplugin.endOfDirectory(addon_handle)

if str(__settings__.getSetting('forceThumbnail')) == "true":
	xbmc.executebuiltin("Container.SetViewMode(500)") # I don't really like this but I did not find any way to set the default layout.
